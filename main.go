package main

import (
	"flag"
	"log"

	"gitlab.com/Boomerangz/minirest/app"
	"gitlab.com/Boomerangz/minirest/config"
)

var (
	configPath *string = flag.String("config", "", "config file path")
)

func init() {
	flag.Parse()

	if configPath != nil && len(*configPath) > 0 {
		config.InitializeConfigFromFile(*configPath)
	}

}

func main() {
	application, err := app.InitApp(config.GetConfig())
	if err != nil {
		log.Fatal(err)
	}

	application.Run()
}
