package transaction

import (
	"fmt"
	"net/http"

	"github.com/kataras/iris"
	"github.com/thedevsaddam/govalidator"
	"gitlab.com/Boomerangz/minirest/app/daos"
	"gitlab.com/Boomerangz/minirest/app/handlers/common"
	"gitlab.com/Boomerangz/minirest/app/models"
)

func Post(ctx iris.Context) {
	var transaction models.Transaction

	rules := govalidator.MapData{
		"amount":       []string{"required"},
		"user_from_id": []string{"required"},
		"user_to_id":   []string{"required"},
	}
	opts := govalidator.Options{
		Request:         ctx.Request(), // request object
		Rules:           rules,         // rules map
		RequiredDefault: true,          // all the field to be pass the rules
		Data:            &transaction,
	}
	v := govalidator.New(opts)
	formatError := v.ValidateJSON()

	if len(formatError) > 0 {
		common.FormatErrorInterface(formatError, http.StatusBadRequest, ctx)
		return
	}

	userDAO := daos.NewUserDAO()
	{
		_, err := userDAO.Get(ctx, uint(transaction.UserFromID))
		if err != nil {
			if err == daos.ErrorNotFound {
				common.FormatErrorInterface(fmt.Sprintf("User with id %d doesn't exists", transaction.UserFromID), http.StatusBadRequest, ctx)
			} else {
				common.FormatError(err, http.StatusInternalServerError, ctx)
			}
			return
		}
	}

	{
		_, err := userDAO.Get(ctx, uint(transaction.UserToID))
		if err != nil {
			if err == daos.ErrorNotFound {
				common.FormatErrorInterface(fmt.Sprintf("User with id %d doesn't exists", transaction.UserToID), http.StatusBadRequest, ctx)
			} else {
				common.FormatError(err, http.StatusInternalServerError, ctx)
			}
			return
		}
	}

	transactionDAO := daos.NewTransactionDAO()
	err := transactionDAO.Create(ctx, &transaction)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponse(FormatTransaction(&transaction), http.StatusCreated, ctx)
}
