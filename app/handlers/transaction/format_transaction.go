package transaction

import (
	"gitlab.com/Boomerangz/minirest/app/models"
	"time"
)

type FormattedTransaction struct {
	ID         uint      `json:"id"`
	UserFromID int64     `json:"user_from_id"`
	UserToID   int64     `json:"user_to_id"`
	Amount     uint64    `json:"amount"`
	CreatedAt  time.Time `json:"created_at"`
}

func FormatTransaction(transaction *models.Transaction) FormattedTransaction {
	return FormattedTransaction{
		ID:         transaction.ID,
		UserFromID: transaction.UserFromID,
		UserToID:   transaction.UserToID,
		Amount:     transaction.Amount,
		CreatedAt:  transaction.CreatedAt,
	}
}

func FormatTransactionList(transactions []*models.Transaction) []FormattedTransaction {
	var formattedTransactions []FormattedTransaction
	formattedTransactions = make([]FormattedTransaction, len(transactions))

	for i, t := range transactions {
		formattedTransactions[i] = FormatTransaction(t)
	}

	return formattedTransactions
}
