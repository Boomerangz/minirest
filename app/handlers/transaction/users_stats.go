package transaction

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	"github.com/kataras/iris"
	"gitlab.com/Boomerangz/minirest/app/daos"
	"gitlab.com/Boomerangz/minirest/app/handlers/common"
	"gitlab.com/Boomerangz/minirest/app/models"
	"gitlab.com/Boomerangz/minirest/config"
)

func UsersStats(ctx iris.Context) {
	config := ctx.Values().Get("config").(func() config.Config)()

	userID, err := getUserID(ctx)
	if err != nil {
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}

	//Get User by ID and throw error is it doesn't exist
	var user *models.User
	userDAO := daos.NewUserDAO()
	{
		user, err = userDAO.Get(ctx, uint(userID))
		if err != nil {
			if err == daos.ErrorNotFound {
				common.FormatError(err, http.StatusNotFound, ctx)
			} else {
				common.FormatError(err, http.StatusInternalServerError, ctx)
			}
			return
		}
	}
	//=======================
	//Set Query for getting transactions only for requested user
	query := "(user_from_id = ? OR user_to_id = ?)"
	queryParams := []interface{}{
		user.ID,
		user.ID,
	}

	//=======================
	//Get offset if it's set
	offset, err := getOffset(ctx)
	if err != nil {
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}

	//=======================
	//Get limit if it's set
	limit, err := getLimit(ctx)
	if err != nil {
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}
	if limit == 0 || limit > config.MaxItemsPerPage {
		limit = config.MaxItemsPerPage
	}

	//=======================
	//Get date_from if it's set and put it to query
	dateFrom, err, dateFromExists := getDate(ctx, "date_from")
	if err != nil {
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}
	if dateFromExists {
		query += " AND created_at >= ?"
		queryParams = append(queryParams, dateFrom)
	}

	//=======================
	//Get date_to if it's set and put it to query
	dateTo, err, dateToExists := getDate(ctx, "date_to")
	if err != nil {
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}
	if dateToExists {
		query += " AND created_at < ?"
		queryParams = append(queryParams, dateTo)
	}

	//Get transactions for our query
	transactionDAO := daos.NewTransactionDAO()
	transactions, err := transactionDAO.Query(ctx, uint64(offset), uint64(limit), query, queryParams...)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	//Get count of transactions for our query
	count, err := transactionDAO.Count(ctx, query, queryParams...)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	//Get count of transactions for our query
	var dateFromLink *time.Time
	if dateFromExists {
		dateFromLink = &dateFrom
	}
	var dateToLink *time.Time
	if dateToExists {
		dateToLink = &dateTo
	}
	inSum, outSum, err := userDAO.IOSum(ctx, user.ID, dateFromLink, dateToLink)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	response := map[string]interface{}{
		"transactions": FormatTransactionList(transactions),
		"income_sum":   inSum,
		"outcome_sum":  outSum,
	}

	common.FormatResponseList(response, count, http.StatusOK, ctx)
}

func getUserID(ctx iris.Context) (userID int64, err error) {
	userIdStr := ctx.FormValue("user_id")
	if userIdStr != "" {
		var err error
		userID, err := strconv.Atoi(userIdStr)

		if err != nil || userID < 0 {
			return 0, errors.New("user_id must be positive integer")
		}

		return int64(userID), nil
	} else {
		return 0, errors.New("user_id is required parameter")
	}
}

func getOffset(ctx iris.Context) (offset uint, err error) {
	offsetStr := ctx.FormValue("offset")
	if offsetStr != "" {
		var err error
		offset, err := strconv.Atoi(offsetStr)

		if err != nil || offset < 0 {
			return 0, errors.New("offset must be positive integer")
		}

		return uint(offset), nil
	}
	return 0, nil
}

func getLimit(ctx iris.Context) (limit uint, err error) {
	limitStr := ctx.FormValue("limit")
	if limitStr != "" {
		var err error
		limit, err := strconv.Atoi(limitStr)

		if err != nil || limit < 0 {
			return 0, errors.New("limit must be positive integer or zero")
		}

		return uint(limit), nil
	}
	return 0, nil
}

func getDate(ctx iris.Context, fieldName string) (date time.Time, err error, exists bool) {
	dateStr := ctx.FormValue(fieldName)
	if dateStr != "" {
		var err error
		date, err := time.Parse("2006-01-02", dateStr)
		return date, err, true
	}
	return date, nil, false

}
