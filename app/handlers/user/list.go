package user

import (
	"net/http"
	"strconv"

	"github.com/kataras/iris"
	"gitlab.com/Boomerangz/minirest/app/daos"
	"gitlab.com/Boomerangz/minirest/app/handlers/common"
	"gitlab.com/Boomerangz/minirest/config"
)

func List(ctx iris.Context) {
	config := ctx.Values().Get("config").(func() config.Config)()

	offsetStr := ctx.FormValue("offset")
	var offset int

	if offsetStr != "" {
		var err error
		offset, err = strconv.Atoi(offsetStr)

		if err != nil || offset < 0 {
			common.FormatErrorInterface("offset must be positive integer", http.StatusBadRequest, ctx)
			return
		}
	}

	limitStr := ctx.FormValue("limit")
	var limit int

	if limitStr != "" {
		var err error
		limit, err = strconv.Atoi(limitStr)

		if err != nil || limit < 0 {
			common.FormatErrorInterface("limit must be positive integer", http.StatusBadRequest, ctx)
			return
		}
	}

	if limit == 0 || limit > int(config.MaxItemsPerPage) {
		limit = int(config.MaxItemsPerPage)
	}

	userDAO := daos.NewUserDAO()
	users, err := userDAO.Query(ctx, uint64(offset), uint64(limit))
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	count, err := userDAO.Count(ctx)
	if err != nil {
		common.FormatError(err, http.StatusInternalServerError, ctx)
		return
	}

	common.FormatResponseList(users, count, http.StatusOK, ctx)
}
