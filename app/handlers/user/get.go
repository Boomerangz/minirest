package user

import (
	"net/http"
	"strconv"

	"github.com/kataras/iris"
	"gitlab.com/Boomerangz/minirest/app/daos"
	"gitlab.com/Boomerangz/minirest/app/handlers/common"
)

func Get(ctx iris.Context) {

	IDStr := ctx.Params().Get("id")
	ID, err := strconv.Atoi(IDStr)
	if err != nil {
		common.FormatError(err, http.StatusBadRequest, ctx)
		return
	}

	userDAO := daos.NewUserDAO()
	user, err := userDAO.Get(ctx, uint(ID))
	if err != nil {
		if err == daos.ErrorNotFound {
			common.FormatError(err, http.StatusNotFound, ctx)
		} else {
			common.FormatError(err, http.StatusInternalServerError, ctx)
		}
		return
	}

	common.FormatResponse(user, http.StatusOK, ctx)
}
