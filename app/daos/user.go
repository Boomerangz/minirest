package daos

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"gitlab.com/Boomerangz/minirest/app/models"
)

// UserDAO persists user data in database
type UserDAO struct {
	BasicDAO
}

// NewUserDAO creates a new UserDAO
func NewUserDAO() *UserDAO {
	return &UserDAO{}
}

// Get reads the user with the specified ID from the database.
func (dao *UserDAO) Get(ctx iris.Context, id uint) (*models.User, error) {
	db := dao.GetDBFromContext(ctx)

	var user models.User
	user.ID = id
	err := db.Find(&user).Error

	if gorm.IsRecordNotFoundError(err) {
		err = ErrorNotFound
	}

	return &user, err
}

// Create saves a new artist record in the database.
// The User.Id field will be populated with an automatically generated ID upon successful saving.
func (dao *UserDAO) Create(ctx iris.Context, user *models.User) error {
	db := dao.GetDBFromContext(ctx)

	user.ID = uint(0)
	err := db.Create(&user).Error
	return err
}

// Update saves the changes to an user in the database.
func (dao *UserDAO) Update(ctx iris.Context, id uint, user *models.User) error {
	if _, err := dao.Get(ctx, id); err != nil {
		return err
	}

	db := dao.GetDBFromContext(ctx)
	user.ID = uint(id)
	return db.Update(&user).Error
}

// Delete deletes an user with the specified ID from the database.
func (dao *UserDAO) Delete(ctx iris.Context, id uint) error {
	user, err := dao.Get(ctx, id)
	if err != nil {
		return err
	}

	db := dao.GetDBFromContext(ctx)
	return db.Delete(&user).Error
}

// Count returns the number of the user records in the database.
func (dao *UserDAO) Count(ctx iris.Context) (uint64, error) {
	db := dao.GetDBFromContext(ctx)

	var count uint64
	err := db.Model(&models.User{}).Count(&count).Error
	return count, err
}

// Query retrieves the artist records with the specified offset and limit from the database.
func (dao *UserDAO) Query(ctx iris.Context, offset, limit uint64) ([]models.User, error) {
	db := dao.GetDBFromContext(ctx)
	users := []models.User{}

	query := db.Model(&models.User{}).Order("id").Offset(offset)

	if limit > 0 {
		query = query.Limit(limit)
	}

	err := query.Find(&users).Error
	return users, err
}

// Income and outcome sum returns the number of the user records in the database.
func (dao *UserDAO) IOSum(ctx iris.Context, userID uint, fromDate *time.Time, toDate *time.Time) (uint64, uint64, error) {
	db := dao.GetDBFromContext(ctx)

	whereCondition := "(user_to_id = ? OR user_from_id = ?) "
	whereParams := []interface{}{userID, userID}

	if fromDate != nil {
		whereCondition += " AND created_at >= ?"
		whereParams = append(whereParams, fromDate)
	}

	if toDate != nil {
		whereCondition += " AND created_at < ?"
		whereParams = append(whereParams, toDate)
	}

	var income, outcome uint64
	query := db.Model(models.Transaction{}).
		Where(whereCondition, whereParams...).
		Select(fmt.Sprintf("SUM(case WHEN user_to_id = %d THEN amount ELSE 0 END), SUM(case WHEN user_from_id = %d THEN amount ELSE 0 END)", userID, userID))
	err := query.Error
	if err != nil {
		return income, outcome, err
	}
	row := query.Row()
	row.Scan(&income, &outcome)
	return income, outcome, err
}
