package daos

import (
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"gitlab.com/Boomerangz/minirest/app/models"
)

// TransactionDAO persists transaction data in database
type TransactionDAO struct {
	BasicDAO
}

// NewTransactionDAO creates a new TransactionDAO
func NewTransactionDAO() *TransactionDAO {
	return &TransactionDAO{}
}

// Get reads the transaction with the specified ID from the database.
func (dao *TransactionDAO) Get(ctx iris.Context, id uint) (*models.Transaction, error) {
	db := dao.GetDBFromContext(ctx)

	var transaction models.Transaction
	transaction.ID = id
	err := db.Find(&transaction).Error

	if gorm.IsRecordNotFoundError(err) {
		err = ErrorNotFound
	}

	return &transaction, err
}

// Create saves a new transaction record in the database.
// The Transaction.Id field will be populated with an automatically generated ID upon successful saving.
func (dao *TransactionDAO) Create(ctx iris.Context, transaction *models.Transaction) error {
	db := dao.GetDBFromContext(ctx)

	transaction.ID = uint(0)
	err := db.Create(&transaction).Error
	return err
}

// Update saves the changes to an transaction in the database.
func (dao *TransactionDAO) Update(ctx iris.Context, id uint, transaction *models.Transaction) error {
	if _, err := dao.Get(ctx, id); err != nil {
		return err
	}

	db := dao.GetDBFromContext(ctx)
	transaction.ID = uint(id)
	return db.Update(&transaction).Error
}

// Delete deletes an transaction with the specified ID from the database.
func (dao *TransactionDAO) Delete(ctx iris.Context, id uint) error {
	transaction, err := dao.Get(ctx, id)
	if err != nil {
		return err
	}

	db := dao.GetDBFromContext(ctx)
	return db.Delete(&transaction).Error
}

// Count returns the number of the transactions in the database.
func (dao *TransactionDAO) Count(ctx iris.Context, whereCondition string, whereParams ...interface{}) (uint64, error) {
	db := dao.GetDBFromContext(ctx)

	query := db.Model(&models.Transaction{})

	if whereCondition != "" {
		query = query.Where(whereCondition, whereParams...)
	}

	var count uint64
	err := query.Count(&count).Error
	return count, err
}

// Query retrieves the artist records with the specified offset and limit from the database.
func (dao *TransactionDAO) Query(ctx iris.Context, offset, limit uint64, whereCondition string, whereParams ...interface{}) ([]*models.Transaction, error) {
	db := dao.GetDBFromContext(ctx)
	transactions := []*models.Transaction{}

	query := db.Model(&models.Transaction{}).Order("id").Offset(offset)

	if whereCondition != "" {
		query = query.Where(whereCondition, whereParams...)
	}

	if limit > 0 {
		query = query.Limit(limit)
	}

	err := query.Find(&transactions).Error
	return transactions, err
}
