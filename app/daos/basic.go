package daos

import (
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
)

type BasicDAO struct{}

func (dao *BasicDAO) GetDBFromContext(ctx iris.Context) *gorm.DB {
	return ctx.Values().Get("db").(func() *gorm.DB)()
}
