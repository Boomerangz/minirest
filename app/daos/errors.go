package daos

import "errors"

var ErrorNotFound error = errors.New("Record not found")
