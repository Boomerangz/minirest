package app

import (
	"fmt"

	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"
	"gitlab.com/Boomerangz/minirest/app/handlers"
	"gitlab.com/Boomerangz/minirest/app/handlers/transaction"
	"gitlab.com/Boomerangz/minirest/app/handlers/user"
	"gitlab.com/Boomerangz/minirest/app/models"
	"gitlab.com/Boomerangz/minirest/config"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type App struct {
	config     *config.Config
	server     *iris.Application
	connection *gorm.DB
}

func InitApp(config *config.Config) (*App, error) {
	connection, err := initDbConnection(config)
	if err != nil {
		return nil, err
	}

	app := &App{
		config:     config,
		connection: connection,
	}
	app.server = iris.New()

	app.InitRouter()

	return app, nil
}

func (a *App) InitRouter() {
	a.server.UseGlobal(func(ctx iris.Context) {
		ctx.Values().Set("db", a.GetDBConnection)
		ctx.Values().Set("config", a.GetConfig)
		ctx.Next() // execute the next handler, in this case the main one.
	})
	if a.config.DebugLog {
		a.server.UseGlobal(logger.New())
	}

	a.server.Get("/healthcheck", handlers.HealthCheck)

	users := a.server.Party("/users")
	users.Get("", user.List)
	users.Post("", user.Post)
	users.Get("/{id:string}", user.Get)

	transactions := a.server.Party("/transactions")
	transactions.Get("", transaction.List)
	transactions.Post("", transaction.Post)
	transactions.Get("/{id:string}", transaction.Get)
	transactions.Get("/user_stats", transaction.UsersStats)
}

func (a *App) Run() {
	// Start the server using a network address.
	a.server.Run(iris.Addr(fmt.Sprintf(":%d", a.config.ServerPort)))
}

func (a *App) Stop() {
	a.server.Shutdown(nil)
}

func (a *App) GetDBConnection() *gorm.DB {
	return a.connection
}

func (a *App) GetConfig() config.Config {
	return *a.config
}

func initDbConnection(appConfig *config.Config) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", appConfig.DBUrl)
	db.AutoMigrate(&models.User{}, &models.Transaction{})
	if appConfig.DebugLog {
		db = db.Debug()
	}
	return db, err
}
