package models

import "github.com/jinzhu/gorm"

type Transaction struct {
	gorm.Model
	UserFromID int64 `json:"user_from_id"`
	UserFrom   User  `json:"user_from"`

	UserToID int64 `json:"user_to_id"`
	UserTo   User  `json:"user_to"`

	Amount uint64 `json:"amount"`
}
