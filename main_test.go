package main

import (
	"testing"
	"time"

	"github.com/google/logger"

	"github.com/jochasinga/requests"
	"gitlab.com/Boomerangz/minirest/app"
	"gitlab.com/Boomerangz/minirest/config"
)

var (
	application *app.App
	testUrl     string = "http://127.0.0.1:8088"
)

func init() {
	config.InitializeConfigFromFile("config_test.json")
	var err error
	application, err = app.InitApp(config.GetConfig())
	time.Sleep(time.Second * 2)
	if err != nil {
		logger.Fatal(err)
	}
	go application.Run()
}

func TestStart(t *testing.T) {
	logger.Info("Started testing")
}

func TestHealthCheck(t *testing.T) {
	res, err := requests.Get(testUrl + "/healthcheck")
	check(err, t)

	if res.StatusCode != 200 {
		t.Errorf("Expected 200 but got %d", res.StatusCode)
	}

	res, err = requests.Get(testUrl + "/not_existing_healthcheck")
	check(err, t)

	if res.StatusCode != 404 {
		t.Errorf("Expected 404 but got %d", res.StatusCode)
	}

}

func TestFinish(t *testing.T) {
	application.Stop()
}

func check(err error, t *testing.T) {
	if err != nil {
		t.Error(err)
	}
}
