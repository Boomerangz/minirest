# Minirest

Golang based small financial transaction storage

### Install
Minirest requires **PostreSQL** to be installed

To install you can execute:

```
git clone https://gitlab.com/Boomerangz/minirest.git
cd minirest
make install
```

### Run

```
make run
```

### Test
```
make test
```


### Configuration

All configs are stored in `configs.json`
There you can change database host, name, etc.

### Using
Minirest provides simple REST API with just a few endpoints


##### To get users list
**GET** ```http://localhost:8080/users/```


##### To add new user
**POST** ```http://localhost:8000/users/``` with body like:
```
{
"username": "Some username"
}
```

You can provide any `username`
***
##### To get single user

**GET** ```http://localhost:8000/users/<id>```
And you will get response like:
```
{
    "response":{
        "id": 1,
        "username": "Some username"
    }
}
```

You **CAN NOT EDIT OR DELETE** user. It is not implemented
***

##### To get transactions list
**GET** ```http://localhost:8080/transactions/```


##### To add new transaction
**POST** ```http://localhost:8000/transactions/``` with body like:
```
{
    "user_from_id":1, 
    "user_to_id":2, 
    "amount":1000
}
```

You can create transaction only for existing users
***
##### To get single transaction

**GET** ```http://localhost:8000/transactions/<id>```
And you will get response like:
```
{
    "response":{
        "id":9,
        "user_from_id":1,
        "user_to_id":2,
        "amount":1000,
        "created_at":"2018-07-06T15:16:03.355159+03:00"
        }
    }
```

You **CAN NOT EDIT OR DELETE** transaction. It is not implemented
***

##### To get users transaction statistics

**GET** ```http://localhost:8000/transactions/user_stats?user_id=<id>&date_from=<date in format 2018-07-06>&date_to=<date in format 2018-07-06>```
And you will get response like:
```
{
    "response":{
        "income_sum":0,
        "outcome_sum":1000,
        "transactions": <transactions_list>
        }
    }
```

