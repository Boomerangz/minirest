fmt:
	gofmt -d -w

test:
	go test

build:
	go build

run:
	go run main.go --config config.json

install:
	glide install